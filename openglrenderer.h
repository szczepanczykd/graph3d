﻿#ifndef OPENGLRENDERER_H
#define OPENGLRENDERER_H

#include <QtCore\qdebug.h>
#include <Renderer.h>
#include <QtOpenGL\QGLWidget>
#include <QtGui\QWheelEvent>
#include <QtGui\QMouseEvent>
#include <scene3d.h>

class Scene3D;

class OpenGLRenderer : public QGLWidget, public Renderer {
	
Q_OBJECT

public:
	OpenGLRenderer(Scene3D *model,QWidget * parent = Q_NULLPTR);
	~OpenGLRenderer();

	void render();
	void initialize();

	void initializeGL();
	void resizeGL(int width, int height);
	void paintGL();
	void wheelEvent(QWheelEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	virtual void show();
	
private:
	
Scene3D * model;

};


#endif // !OPENGLRENDERER_H