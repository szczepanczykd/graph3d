﻿#pragma once
#include "QtWidgets\qwidget.h"
#include <QtWidgets\qlayout.h>
#include <QtWidgets\qcheckbox.h>
#include <QtWidgets\qlabel.h>
#include <QtCore\qdebug.h>

class meshProperties : public QWidget {
	Q_OBJECT
		bool _visible;
		QString _ownerName;
		QCheckBox *visibleBox = 0;
public:
	meshProperties();
	meshProperties(const meshProperties& meshProp);
	meshProperties(QString name,QWidget * parent = Q_NULLPTR);
	~meshProperties();
	QString getName() const;
	bool visible() const;
	meshProperties& operator= (const meshProperties& meshProp);
private:
	void createCheckBox();
	signals:
	void stateChanged(const QString _objName, bool state);
	private slots:
	void checkboxPressed(bool state);
};
