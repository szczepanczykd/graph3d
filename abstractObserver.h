#pragma once

class abstractObserver {

public:
	virtual void update() = 0;

};