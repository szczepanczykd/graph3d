#ifndef OBJECT3DFACTORY_H
#define OBJECT3DFACTORY_H

#include "QtWidgets\qwidget.h"
#include "mesh3d.h"
#include "point3d.h"
#include "triangle3d.h"
#include <QtGui\qvector3d.h>
#include <QtWidgets\qlineedit.h>
#include <QtWidgets\qlabel.h>
#include <meshparamseditor.h>
#include <QtWidgets\qpushbutton.h>
#include <QtWidgets\qlayout.h>
#include <QtGui\qcolor.h>



class Object3DFactory : public QWidget
{
	Q_OBJECT

	QLineEdit *first = 0;
	QLineEdit *second = 0;
	QLineEdit *third = 0;

	QPushButton *openSettings = 0;
	QPushButton *commit = 0;

	MeshParamsEditor *editor = 0;

	QString _objName;

	QVector3D _offset;
	QVector3D _move;
	QVector3D _degrees;

	QColor _color;

public:
	Object3DFactory(QString objName,QWidget *parent = 0);
	~Object3DFactory();
   static Mesh3D CreateCube(float x , float y, float z,QString objName);
   static Mesh3D CreateCone(float r ,float h, int x,QString objName);

signals:
   void objectCreated(const Mesh3D &val);

  private slots:
  void openParamsEditor();
  void commitPressed();
  void setValues(const QString objName, const QVector3D &offset, const QVector3D &move, const QVector3D &degrees, const QColor &color);

};

#endif // OBJECT3DFACTORY_H
