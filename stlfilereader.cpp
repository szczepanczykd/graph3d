#include "stlfilereader.h"

STLFileReader::STLFileReader()
{

}

STLFileReader::~STLFileReader()
{

}

bool STLFileReader::readFromFile(const QString &filename, Scene3D *&scene)
{

    QFile file(filename.toStdString().c_str());
	QByteArray bytearray;
	Mesh3D retVal(filename);
	unsigned int triangleNum = 0;


    if (!file.open(QIODevice::ReadOnly))
    return false;
	
 
	//Go to 80 byte, and if is okay, go forward.
	if (file.seek(80) == false)
	{
		qDebug() << "False for 80";
		file.close();
		return false;
	}


	//Reading num of triangles.
    bytearray = file.read(4);
    memcpy(&triangleNum,bytearray.constData(),4);

	//Reading size of file (for validate schema)
	qint64 size = file.size();
	qDebug() << 84 + triangleNum * 50 << " bytes num, and byte size: " << size;

	//Wrong proto, or data is corrupted !
	if (84 + triangleNum * 50 != size)
	{
		qDebug() << "False! for num triangles";
		file.close();
		return false;
	}


    while(triangleNum--)
    {
	
    bytearray = file.read(50);
    float xyz[12];

    memcpy(&xyz,bytearray.constData(),48);


	for (int i = 0; i < 12; i+=3)
	{
		Point3D out(xyz[i], xyz[i + 1], xyz[i + 2]);
		retVal.insertPoint(out);
	}

    }




	//done. We need to close file, and...
	file.close();
	//...insert object to scene, and....
	scene->insertMeshObject(retVal);
	//return info. "OK"
	return true;
}

