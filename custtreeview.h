#ifndef CUSTTREEVIEW_H
#define CUSTTREEVIEW_H

#include <QtCore\qobject.h>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\QPushButton>
#include <QtWidgets\QTreeView>
#include <QtGui\QStandardItemModel>
#include <QtGui\QStandardItem>
#include <QtWidgets\QHBoxLayout>
#include <QtWidgets\QVBoxLayout>
#include <QtCore\QDebug>
#include "meshparamseditor.h"
#include <QtGui\QMouseEvent>
#include <QtCore\qscopedpointer.h>
#include <QtWidgets\qscrollarea.h>
#include <QtWidgets\qheaderview.h>
#include "meshproperties.h"

#include "abstractSubject.h"
#include "abstractObserver.h"
class TreeView;

class CustTreeView : public QWidget, public abstractSubject
{
	Q_OBJECT
		QList<abstractObserver*> observerList;
	
    TreeView *treeView = 0;
    QStandardItemModel *model = 0;
    QList<QStandardItem *> rows;
	QVector<meshProperties> meshPropVec;
    QList<QVector<QStandardItem *>> objects;
    QStandardItem *item = 0;
	MeshParamsEditor *meshEditor = 0;
	meshProperties propEdit;
	QColor _color;
public:
    explicit CustTreeView(QWidget *parent = 0);
	~CustTreeView();
    void addItem(const QString cathegory, QString itemName);

	void attach(abstractObserver *o);
	void detach(abstractObserver *o);
	void notify();
	QColor action();

signals:
	void requiredEditor(const QString &name);
	void valuesUpdated(const QString objName, const QVector3D &offset, const QVector3D &move, const QVector3D &degrees, const QColor &color);
	void requiredMeshUpdate(const QString updateName);
	void setParamsSig(const QString objName, bool visible);
public slots:
    void openEditor(const QString selName,bool leftButton);
    void purgeTree();
	void valuesSetted(const QString objName, const QVector3D &offset, const QVector3D &move, const QVector3D &degrees, const QColor &color);
	void setParams(const QString objName, bool visible);
};

class TreeView : public QTreeView
{
	Q_OBJECT
public:
	TreeView(QWidget *parent = 0);
protected:
	void mouseDoubleClickEvent(QMouseEvent *event);
signals:
	void selected(const QString selName, bool leftButton);

};

#endif // CUSTTREEVIEW_H
