#pragma once

#include "abstractObserver.h"

class abstractSubject {

public:
	virtual void attach(abstractObserver*) = 0;
	virtual void detach(abstractObserver*) = 0;
	virtual void notify() = 0;
	virtual QColor action() = 0;
};

