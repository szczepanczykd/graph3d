#include "object3dfactory.h"

#define PI 3.14159265

Object3DFactory::Object3DFactory(QString objName, QWidget * parent) : _objName(objName),QWidget(parent)
{
	QVBoxLayout *vbox = new QVBoxLayout(this);
	QHBoxLayout *hbox = new QHBoxLayout(this);
	QHBoxLayout *hboxSec = new QHBoxLayout(this);
	
	first = new QLineEdit(this);
	second = new QLineEdit(this);
	third = new QLineEdit(this);

	openSettings = new QPushButton("Figure settings",this);
	commit = new QPushButton("Add item!",this);

	QLabel *descFirst = new QLabel("x", this);
	QLabel *descSec = new QLabel("y", this);
	QLabel *descThi = new QLabel("z", this);


	descFirst->setAlignment(Qt::AlignCenter);
	descSec->setAlignment(Qt::AlignCenter);
	descThi->setAlignment(Qt::AlignCenter);

	hbox->addWidget(descFirst);
	hbox->addWidget(descSec);
	hbox->addWidget(descThi);

	vbox->addLayout(hbox);

	hboxSec->addWidget(first);
	hboxSec->addWidget(second);
	hboxSec->addWidget(third);

	vbox->addLayout(hboxSec);

	vbox->addWidget(openSettings);
	vbox->addWidget(commit);

	setLayout(vbox);

	connect(openSettings, SIGNAL(pressed()), this, SLOT(openParamsEditor()));
	connect(commit, SIGNAL(pressed()), this, SLOT(commitPressed()));

	if (objName == "Cone")
	{
		third->setText("3");
		descFirst->setText("r");
		descSec->setText("h");
		descThi->setText("Number of edges");
		first->setValidator(new QDoubleValidator(0, 1000000, 5));
		second->setValidator(new QDoubleValidator(-10000000, 1000000, 5));
		third->setValidator(new QIntValidator(2, 3600));
	}
	else
	{
		first->setValidator(new QDoubleValidator(-10000000, 1000000, 5));
		second->setValidator(new QDoubleValidator(-10000000, 1000000, 5));
		third->setValidator(new QDoubleValidator(-10000000, 1000000, 5));
	}

}

Object3DFactory::~Object3DFactory()
{
	if (editor)
		delete editor;
}

Mesh3D Object3DFactory::CreateCube(float x, float y, float z, QString objName)
{
Mesh3D retMesh(objName);

//Map of points (for every single triangle)


retMesh.insertPoint(Point3D(0, 0, -1)); //front
retMesh.insertPoint(Point3D(0, 0, 0));
retMesh.insertPoint(Point3D(1 * x, 0, 0));
retMesh.insertPoint(Point3D(1 * x, 1 * y, 0));

retMesh.insertPoint(Point3D(0, 0, -1));
retMesh.insertPoint(Point3D(retMesh.pointAt(3)));
retMesh.insertPoint(Point3D(retMesh.pointAt(1)));
retMesh.insertPoint(Point3D(0 * x, y, 0));

retMesh.insertPoint(Point3D(0,  -1, 0)); //Up
retMesh.insertPoint(Point3D(retMesh.pointAt(3)));
retMesh.insertPoint(Point3D(retMesh.pointAt(7)));
retMesh.insertPoint(Point3D(0 * x, 1 * y, 1*z));

retMesh.insertPoint(Point3D(0, -1, 0));
retMesh.insertPoint(Point3D(0 * x, 1 * y, 1 * z));
retMesh.insertPoint(Point3D(1 * x, 1 * y, 0));
retMesh.insertPoint(Point3D(1 * x, 1 * y, 1 * z));

retMesh.insertPoint(Point3D(1, 0, 0)); //right
retMesh.insertPoint(Point3D(1 * x, 1 * y, 1 * z));
retMesh.insertPoint(Point3D(1 * x, 1 * y, 0));
retMesh.insertPoint(Point3D(1 * x, 0 * y, 1 * z));

retMesh.insertPoint(Point3D(1, 0, 0));
retMesh.insertPoint(Point3D(1 * x, 0 * y, 0 * z));
retMesh.insertPoint(Point3D(1 * x, 1 * y, 0));
retMesh.insertPoint(Point3D(1 * x, 0 * y, 1 * z));

retMesh.insertPoint(Point3D(-1, 0, 0)); //left
retMesh.insertPoint(Point3D(0 * x, 1 * y, 1 * z));
retMesh.insertPoint(Point3D(0 * x, 1 * y, 0));
retMesh.insertPoint(Point3D(0 * x, 0 * y, 1 * z));

retMesh.insertPoint(Point3D(-1, 0, 0));
retMesh.insertPoint(Point3D(0 * x, 0 * y, 0 * z));
retMesh.insertPoint(Point3D(0 * x, 1 * y, 0));
retMesh.insertPoint(Point3D(0 * x, 0 * y, 1 * z));

retMesh.insertPoint(Point3D(0, 0, 1)); //back
retMesh.insertPoint(Point3D(0, 0, z * 1));
retMesh.insertPoint(Point3D(1 * x, 0, z * 1));
retMesh.insertPoint(Point3D(1 * x, 1 * y, z * 1));

retMesh.insertPoint(Point3D(0, 0, 1));
retMesh.insertPoint(Point3D(0, 0, z * 1));
retMesh.insertPoint(Point3D(0 * x, 1 * y, z * 1));
retMesh.insertPoint(Point3D(1 * x, 1 * y, z * 1));

retMesh.insertPoint(Point3D(0, 1, 0)); //down
retMesh.insertPoint(Point3D(0 * x, 0 * y, 1 * z));
retMesh.insertPoint(Point3D(1 * x, 0 * y, 0));
retMesh.insertPoint(Point3D(1 * x, 0 * y, 1 * z));

retMesh.insertPoint(Point3D(0, 1, 0));
retMesh.insertPoint(Point3D(0 * x, 0 * y, 0));
retMesh.insertPoint(Point3D(0 * x, 0 * y, 1 * z));
retMesh.insertPoint(Point3D(1 * x, 0 * y, 0));	


return retMesh;
}

Mesh3D Object3DFactory::CreateCone(float r, float h, int x, QString objName)
{
	Mesh3D retVal(objName);

	if (x < 2)
		x = 2;

	float degree = 360 / x;
	qDebug() << degree;
	for (int i = 0; i < 360 / degree; i++)
	{
		retVal.insertPoint(Point3D(0, 0, 0));
		retVal.insertPoint(Point3D( cos((degree * PI / 180) * i) * r, 0, sin((degree * PI / 180)  * i) * r));
		retVal.insertPoint(Point3D(cos((degree * PI / 180)  * (i + 1) ) * r, 0, sin((degree * PI / 180)  * ( i + 1)) * r));
		retVal.insertPoint(Point3D(0, h, 0));
	}
	
	return retVal;
}

void Object3DFactory::openParamsEditor()
{
	
	if (editor)
	delete editor;

	editor = new MeshParamsEditor(_objName);
	editor->show();
	connect(editor, SIGNAL(valuesSetted(const QString, const QVector3D &, const QVector3D &, const QVector3D &, const QColor &)), this, SLOT(setValues(const QString, const QVector3D &, const QVector3D &, const QVector3D &, const QColor &)));
}

void Object3DFactory::commitPressed()
{
	
	Mesh3D retVal;

	if (_objName == "Cube")
	{
		retVal = Object3DFactory::CreateCube(first->text().toFloat(),second->text().toFloat(), third->text().toFloat(), "General/Cube");
		retVal.insertOffset(_offset);
		retVal.setColor(_color);
		retVal.setDegrees(_degrees);
		retVal.setMove(_move);
	}
	else if (_objName == "Cone")
	{
		retVal = Object3DFactory::CreateCone(first->text().toFloat(), second->text().toFloat(), third->text().toFloat(), "General/Cone");
		retVal.insertOffset(_offset);
		retVal.setColor(_color);
		retVal.setDegrees(_degrees);
		retVal.setMove(_move);
	}
	else
	{
		return;
	}

	qDebug() << retVal.getName();

	emit objectCreated(retVal);
}

void Object3DFactory::setValues(const QString objName, const QVector3D & offset, const QVector3D & move, const QVector3D & degrees, const QColor & color)
{
	this->_offset = offset;
	this->_move = move;
	this->_degrees = degrees;
	this->_color = color;
}


