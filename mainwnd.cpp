#include "mainwnd.h"
/*
All connections between widgets (custtreeview <-> scene3D)
can be found there.


*/

MainWnd::MainWnd(QWidget *parent) :
    QMainWindow(parent)
{
	this->resize(1200, 700);

	QGridLayout *hbox = new QGridLayout();
	QGridLayout *vbox = new QGridLayout();
	this->scene = new Scene3D;
	this->custTreeView = new CustTreeView(this);
	this->renderer = new OpenGLRenderer(scene,this);
	this->scene->setView(renderer);
	
	this->createCone = new QPushButton("Create Cone", this);
	this->createCube = new QPushButton("Create Cube", this);

	QAction *OpenFile = new QAction("&OpenFile", this);
	QAction *Clear = new QAction("&Clear", this);

	mapper = new QSignalMapper(this);
	//QWidget object will be centalwidget..
	//QMainWindow class can't accept any layout (it currently has one).
	//So layout will be part of QWidget, and QMainWindow will set it to centralWidget.
	QWidget *wnd = new QWidget();

	QMenu * _file;
	QMenu *_view;

    _file = menuBar()->addMenu("&File");
	_view = menuBar()->addMenu("&View");

	_file->addAction(OpenFile);
    _view->addAction(Clear);

	dynamic_cast<OpenGLRenderer*> (this->renderer)->QGLWidget::setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	this->createCone->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	this->createCube->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	this->custTreeView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);


   connect (OpenFile,SIGNAL(triggered(bool)),this,SLOT(selectFile()));
   connect(Clear, SIGNAL(triggered(bool)), this->scene, SLOT(purgeItems()));

   connect(this->createCone, SIGNAL(pressed()), mapper, SLOT(map()));
   connect(this->createCube, SIGNAL(pressed()), mapper, SLOT(map()));
   //QSignalMapper is allows to connect multiple element to single slot.
   //Best
   //Thing
   //Ever
   mapper->setMapping(this->createCone, "Cone");
   mapper->setMapping(this->createCube, "Cube");

   connect(mapper, SIGNAL(mapped(QString)), this->scene, SLOT(addFactoryObject(QString)));

   connect(this,SIGNAL(fileChoosen(QString)),this,SLOT(appendItemToTree(QString)));

   connect(this->scene, SIGNAL(itemAdded(QString)), this, SLOT(appendItemToTree(QString)));
   connect(this->scene,SIGNAL(itemsPurged()), this->custTreeView,SLOT(purgeTree()));

   connect(this->custTreeView, SIGNAL(requiredMeshUpdate(const QString)), this->scene, SLOT(updateLastObjName(const QString)));
   connect(this->custTreeView, SIGNAL(valuesUpdated(const QString, const QVector3D&, const QVector3D&, const QVector3D&, const QColor&)), this->scene, SLOT(updateMeshParams(const QString, const QVector3D&, const QVector3D&, const QVector3D&, const QColor&)));
   connect(this->custTreeView, SIGNAL(setParamsSig(const QString, bool)), this->scene, SLOT(setParams(const QString, bool)));

   vbox->addWidget(this->custTreeView,0,0);
   vbox->addWidget(this->createCone,1,0);
   vbox->addWidget(this->createCube,2,0);

   hbox->addLayout(vbox, 0, 0);
   hbox->setColumnStretch(0, 2);
   hbox->addWidget(dynamic_cast<QGLWidget*> (renderer), 0, 1);
   hbox->setColumnStretch(1, 5);

   wnd->setLayout(hbox);

   this->setCentralWidget(wnd);
 
   scene->setSubject(custTreeView);
   custTreeView->attach(scene);

}

MainWnd::~MainWnd()
{
    if(mapper)
    delete mapper;
	delete scene;
}

void MainWnd::selectFile()
{
        QString filedir = QFileDialog::getOpenFileName(this,tr("Open File!"),"", "STL Binary file (*.stl)");
		
        if(filedir != "")
        {
        STLFileReader fileReader;
		if (fileReader.readFromFile(filedir, this->scene))
		{
			//If filePath is okay, we will send signal to custTreeView.
			//It will add a path and object name to control.
			emit fileChoosen(filedir);
		}
		else
		{
			//If not... 
			//There will nothing happen.
			//Maybe QMessageBox should blink?.
			qDebug() << "Failed!";
		}
        }

}

void MainWnd::appendItemToTree(const QString fileDir)
{
   // ui->custTreeView->addItem(QFileInfo(fileDir).path(),QFileInfo(fileDir).fileName());
	this->custTreeView->addItem(QFileInfo(fileDir).path(), QFileInfo(fileDir).fileName());
}

