﻿#pragma once
#include "QtWidgets\qwidget.h"
#include <QtCore\qdebug.h>
#include <QtGui\qcolor.h>
#include <QtGui\qvector3d.h>
#include <QtWidgets\qcolordialog.h>
#include <QtWidgets\qboxlayout.h>
#include <QtWidgets\qpushbutton.h>
#include <QtWidgets\qlabel.h>
#include <QtWidgets\qlineedit.h>
#include <QtWidgets\qsplitter.h>
#include <QtGui\QDoubleValidator>

class MeshParamsEditor : public QWidget {
	Q_OBJECT

public:
	MeshParamsEditor(const QString objName, QWidget * parent = Q_NULLPTR);
	~MeshParamsEditor();

private:
	QString _objName;
	QColor _color;
	QLineEdit *xOffset;
	QLineEdit *yOffset;
	QLineEdit *zOffset;

	QLineEdit *xDegree;
	QLineEdit *yDegree;
	QLineEdit *zDegree;
	
	QLineEdit *xMove;
	QLineEdit *yMove;
	QLineEdit *zMove;

	private slots:
	void getColor();
	void commited();
signals:
	void valuesSetted(const QString objName, const QVector3D &offset, const QVector3D &move, const QVector3D &degrees, const QColor &color);
};
