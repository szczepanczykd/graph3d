#ifndef ABSTRACTFILEREADER_H
#define ABSTRACTFILEREADER_H


#include <QtCore\qstring.h>
#include <QtWidgets\qwidget.h>
#include "scene3d.h"

class AbstractFileReader
{
public:
     virtual bool readFromFile(const QString &filename, Scene3D *&scene) = 0;

};

#endif // ABSTRACTFILEREADER_H
