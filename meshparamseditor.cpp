﻿#include "meshparamseditor.h"

MeshParamsEditor::MeshParamsEditor(const QString objName,QWidget * parent) : QWidget(parent) {
	
	this->_objName = objName;
	QVBoxLayout *vbox = new QVBoxLayout(this);
	QSplitter *splitter1 = new QSplitter(Qt::Horizontal);
	QSplitter *splitter2 = new QSplitter(Qt::Horizontal);
	QSplitter *splitter3 = new QSplitter(Qt::Horizontal);

	QPushButton *colorSelector = new QPushButton("Open Color selector!", this);

	QPushButton * commit = new QPushButton("Apply!", this);

	QLabel *describe = new QLabel("Offset x,y,z", this);
	QLabel *describeSec = new QLabel("Move x,y,z", this);
	QLabel *describeThi = new QLabel("Rotate x,y,z", this);
	
	xOffset = new QLineEdit("0", this);
	yOffset = new QLineEdit("0", this);
	zOffset = new QLineEdit("0", this);

	xMove = new QLineEdit("0", this);
	yMove = new QLineEdit("0", this);
	zMove = new QLineEdit("0", this);

	xDegree = new QLineEdit("0", this);
	yDegree = new QLineEdit("0", this);
	zDegree = new QLineEdit("0", this);

	QList<int> sizes;

	vbox->setSpacing(5);
	
	sizes << 100 << 50;
	
	splitter1->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
	splitter2->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	splitter3->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

	colorSelector->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);

	commit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);

	describe->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
	describeSec->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
	describeThi->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);

	xOffset->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	yOffset->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	zOffset->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

	xMove->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	yMove->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	zMove->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

	xDegree->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	yDegree->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	zDegree->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

	splitter1->setSizes(sizes);
	splitter2->setSizes(sizes);
	splitter3->setSizes(sizes);

	connect(colorSelector, SIGNAL(pressed()), this, SLOT(getColor()));

	describe->setAlignment(Qt::AlignCenter);	
	describeSec->setAlignment(Qt::AlignCenter);
	describeThi->setAlignment(Qt::AlignCenter);
	
	xOffset->setValidator(new QDoubleValidator(-10000000, 1000000, 5));	
	yOffset->setValidator(new QDoubleValidator(-10000000, 1000000, 5));	
	zOffset->setValidator(new QDoubleValidator(-10000000, 1000000, 5));

	xMove->setValidator(new QDoubleValidator(-10000000, 1000000, 5));	
	yMove->setValidator(new QDoubleValidator(-10000000, 1000000, 5));
	zMove->setValidator(new QDoubleValidator(-10000000, 1000000, 5));
		
	xDegree->setValidator(new QIntValidator(-360, 360));
	yDegree->setValidator(new QIntValidator(-360, 360));
	zDegree->setValidator(new QIntValidator(-360, 360));
	

	splitter1->setSizes(sizes);
	splitter2->setSizes(sizes);
	splitter3->setSizes(sizes);

	vbox->addWidget(colorSelector);
	vbox->addWidget(describe);

	splitter1->addWidget(xOffset);
	splitter1->addWidget(yOffset);
	splitter1->addWidget(zOffset);

	vbox->addWidget(splitter1);
	vbox->addWidget(describeSec);

	splitter2->addWidget(xMove);
	splitter2->addWidget(yMove);
	splitter2->addWidget(zMove);

	vbox->addWidget(splitter2);
	vbox->addWidget(describeThi);

	splitter3->addWidget(xDegree);
	splitter3->addWidget(yDegree);
	splitter3->addWidget(zDegree);

	vbox->addWidget(splitter3);
	vbox->addWidget(commit);

	vbox->addStretch(3);
	
	setLayout(vbox);


	connect(commit, SIGNAL(pressed()), this, SLOT(commited()));
	//when button is pressed, we will take all values of qlineedit fields.
}

MeshParamsEditor::~MeshParamsEditor() {
	delete xOffset;
	delete yOffset;
	delete zOffset;
	delete xDegree;
	delete yDegree;
	delete zDegree;
	delete xMove;
	delete yMove;
	delete zMove;
	//returning mem. to OS.
}

void MeshParamsEditor::getColor()
{
	this->_color = QColorDialog::getColor(Qt::white, this, "Select Color");
	//Opening color dialogue.
	//if cancel button is pressed, color will be in invalid state.
}

void MeshParamsEditor::commited()
{
	this->close();
    emit valuesSetted(_objName, QVector3D(xOffset->text().toFloat(), yOffset->text().toFloat(), zOffset->text().toFloat()), QVector3D(xMove->text().toFloat(), yMove->text().toFloat(), zMove->text().toFloat()), QVector3D(xDegree->text().toFloat(), yDegree->text().toFloat(), zDegree->text().toFloat()), this->_color);
	//emitting offset, move, degrees and color values.
}
