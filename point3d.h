#ifndef POINT3D_H
#define POINT3D_H

#include <QtGui\qvector3d.h>

class Point3D : public QVector3D
{
public:
    Point3D();
    Point3D(float x, float y, float z);

};

#endif // POINT3D_H
