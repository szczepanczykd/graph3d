#include "mainwnd.h"
#include <QtWidgets/QApplication>
#include <QtCore/qdebug.h>
#include "triangle3d.h"
#include "object3dfactory.h"
#include <QtWidgets/QWidget>
#include <openglrenderer.h>
#include "scene3d.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
   MainWnd w;
   w.show();

    return a.exec();
}
