#include "triangle3d.h"

/*
We need to store integer values in QVector3D.
That's why there is an conversion from float to int. (and reverse)
*/


Triangle3D::Triangle3D()
{

}

Triangle3D::Triangle3D(int x, int y, int z)
{
    this->setX(x);
    this->setY(y);
    this->setZ(z);
}

int Triangle3D::x()
{
   return (int) QVector3D::x();

}

int Triangle3D::y()
{
    return (int) QVector3D::y();
}

int Triangle3D::z()
{
    return (int) QVector3D::z();
}

void Triangle3D::setX(int x)
{
    QVector3D::setX(x);
}

void Triangle3D::setY(int y)
{
   QVector3D::setY(y);
}
void Triangle3D::setZ(int z)
{
    QVector3D::setZ(z);
}
