#ifndef MESH3D_H
#define MESH3D_H

#include <QtCore\QObject>
#include <QtCore\QVector>
#include <QtGui\QVector3D>
#include <QtGui\QColor>
#include <QtOpenGL\QGLWidget>
#include <QtCore\QDebug>
#include "point3d.h"
#include "triangle3d.h"


class Mesh3D
{
protected:
    QVector <Point3D> _point3DVec;
    QVector <Triangle3D> _triangle3DVec;
    QVector3D _offset;
	QVector3D _move;
	QVector3D _degrees;
    QColor _objectColor;
    QString _objName;
    bool state;

public:
	Mesh3D();
    Mesh3D(const QString name);
    virtual void render();
    void insertPoint(const Point3D &point);
    void insertTriangle(const Triangle3D &triangle);
    void insertOffset(const QVector3D &offset);
    void setName(const QString &objName);
    void setState(bool state);
	void setColor(QColor color);
	void setOffset(QVector3D offset);
	void setDegrees(QVector3D degrees);
	void setMove(QVector3D move);
	Point3D pointAt(int i);
    bool isEnabled();
	QString getName();
};

#endif // MESH3D_H
