#include "custtreeview.h"


CustTreeView::CustTreeView(QWidget *parent) : QWidget(parent)
{
meshEditor = 0;

treeView = new TreeView();
model = new QStandardItemModel;
QHBoxLayout *hbox = new QHBoxLayout(this);

treeView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
//This is a very important thing.
//PATH to file will be adjusted to length.
//If not: scrollarea will show up.
treeView->header()->setStretchLastSection(false);

item = model->invisibleRootItem();

//Layout's is a good idea.
//Always.
hbox->addWidget(treeView);

treeView->setModel(model);
treeView->expandAll();
treeView->show();

this->setLayout(hbox);

connect(treeView, SIGNAL(selected(const QString, bool)), this, SLOT(openEditor(const QString, bool)));
}

CustTreeView::~CustTreeView()
{
	if(meshEditor)
	delete meshEditor;
	if (model)
	delete model;
	if (treeView)
	delete treeView;
	
}

void CustTreeView::addItem(const QString cathegory, QString itemName)
{
	//We need to check column names (cathegories)
	//Put item into new, or existing cathegory
	//And replace itemName, if is already in use :)
	//algorithm complexity is very bad...
    for(int i = 0; i < rows.size(); ++i)
    {
        if(rows.at(i)->text() == cathegory)
       {
		   
		QVector<QStandardItem *> pointer =  objects.at(i);

		int helperCounter = 0;
		QString helpingString = "";
		for (QStandardItem * x : pointer)
		{
			if (x->text() == itemName)
			{
				//name(1)
				helpingString = itemName + "(" + QString::number(++helperCounter) + ")";
			}
			if (x->text() == helpingString)
			{
				//if name(1) is existing.
				//name(2)
				//name(3)
				//etc.
				helpingString = itemName + "(" + QString::number(++helperCounter) + ")";
			}
		}
		if (helpingString != "")
		{
			emit requiredMeshUpdate(cathegory+"/"+helpingString);
			itemName = helpingString;
		}

		pointer.push_back(new QStandardItem(itemName));
		objects.replace(i, pointer);
        rows.at(i)->appendRow(objects.at(i).last());
        treeView->setModel(model);
        treeView->expandAll();

		//I pushed back simple editor. (visible).
		//Access by right mouse button double-click.
		this->meshPropVec.push_back(meshProperties(cathegory + "/" + itemName));

        return ;
        }
    }

    rows.push_back( new QStandardItem (cathegory));
	QVector<QStandardItem *> vec;
	vec.push_back(new QStandardItem(itemName));
    objects.push_back(vec);
    item->appendRow(rows.last());
    rows.last()->appendRow(objects.last().last());
    treeView->setModel(model);
    treeView->expandAll();
	treeView->header()->setSectionResizeMode(treeView->header()->count() - 1, QHeaderView::ResizeToContents);

	this->meshPropVec.push_back(meshProperties(cathegory + "/" + itemName));
}

void CustTreeView::attach(abstractObserver *o)
{
	observerList.push_back(o);
}

void CustTreeView::detach(abstractObserver *o)
{
	for (int i = 0; i < observerList.size(); i++)
	{
		if (o == observerList.at(i))
		{
			observerList.removeAt(i);
			break;
		}
	}
}

void CustTreeView::notify()
{
	for (abstractObserver *x : observerList)
	{
		x->update();
	}
}

QColor CustTreeView::action()
{
	return _color;
}




void CustTreeView::openEditor(const QString selName, bool leftButton)
{
	//if mouse button is twice pressed to item name!
	//we got item name, but cathegory is unknown
	//we have to find it!.

	for (int i = 0; i < objects.size(); i++)
	{
		QVector<QStandardItem *> pointer = objects.at(i);

		for (QStandardItem * x : pointer)
		{
			if (x->text() == selName)
			{
				qDebug() << rows.at(i)->text() + "/" + selName;

				if (leftButton) //if left button were clicked, we will open meshEditor (move, degree, color and offset params).
				{
					if (meshEditor)
					{
						delete meshEditor;
						meshEditor = 0;
					}
					meshEditor = new MeshParamsEditor(rows.at(i)->text() + "/" + selName);
					connect(meshEditor, SIGNAL(valuesSetted(const QString, const QVector3D &, const QVector3D &, const QVector3D &, const QColor &)), this, SLOT(valuesSetted(const QString, const QVector3D&, const QVector3D &, const QVector3D &, const QColor&)));
					meshEditor->show();
				}
				else //if right or wheel? Visible menu!.
				{
					
					for (int z = 0; z < meshPropVec.size(); z++)
					{
						qDebug() << meshPropVec.at(z).getName() << ":vs:" << rows.at(i)->text() + "/" + selName;
						if (meshPropVec.at(z).getName() == rows.at(i)->text() + "/" + selName)
						{									//propEdit is meshProperties class object.
							propEdit = meshPropVec.at(z); //overloaded =operator called here.
							propEdit.show();
							connect(&propEdit, SIGNAL(stateChanged(const QString, bool)), this, SLOT(setParams(const QString, bool)));
							break;
						}
					}
				}

			}
		}
	}

}

void CustTreeView::setParams(const QString objName, bool visible)
{
	
	//visible value in propEdit has been changed.
	//so we need to replace element at vector.

	for (int z = 0; z < meshPropVec.size(); z++)
	{

		if (meshPropVec.at(z).getName() == objName)
		{
			meshPropVec.removeAt(z);
			meshPropVec.insert(z,propEdit);
			break;
		}
	}

	//fire next event.
	emit setParamsSig(objName, visible);
}

void CustTreeView::purgeTree()
{
    //purge everything!

	this->meshPropVec.clear();
    this->rows.clear();
    this->objects.clear();
    model->clear();
    treeView->setModel(model);
    treeView->expandAll();
	//actual rootItem is invalid.
	//we need to reload it.
    item = model->invisibleRootItem();

}

void CustTreeView::valuesSetted(const QString objName, const QVector3D &offset, const QVector3D &move, const QVector3D &degrees, const QColor &color)
{
	//from meshParamsEditor to this.
	//from this to scene3D (connected in mainwindow).
	//I dont want to connect these signals directly by pointer.
	_color = color;
	this->notify();
	emit valuesUpdated(objName, offset,move,degrees, color);
}

TreeView::TreeView(QWidget * parent) : QTreeView(parent)
{

}

void TreeView::mouseDoubleClickEvent(QMouseEvent * event)
{
	if (event->buttons() == Qt::LeftButton)
	{
		QModelIndexList list = this->selectionModel()->selectedRows();
		if (list.size())
		{
			this->selectionModel()->clearSelection();
			emit selected(list.first().data().toString(),true);
		}
	}
	else if (event->buttons() == Qt::RightButton)
	{
		QModelIndexList list = this->selectionModel()->selectedRows();
		if (list.size())
		{
			this->selectionModel()->clearSelection();
			emit selected(list.first().data().toString(), false);
		}
	}
}

