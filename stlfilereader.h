#ifndef STLFILEREADER_H
#define STLFILEREADER_H

#include "abstractfilereader.h"
#include "mesh3d.h"
#include <QtCore\QFile>
#include <QtCore\QScopedPointer>
#include <QtCore\QDebug>
#include <QtCore\QDataStream>

class STLFileReader : public AbstractFileReader
{
public:
    explicit STLFileReader();
    virtual ~STLFileReader();
    bool readFromFile(const QString &filename, Scene3D *&scene);
};

#endif // STLFILEREADER_H
