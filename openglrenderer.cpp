﻿#include "openglrenderer.h"


OpenGLRenderer::OpenGLRenderer(Scene3D *model,QWidget * parent) : model(model), QGLWidget(QGLFormat(QGL::SampleBuffers),parent)
{
	
}

OpenGLRenderer::~OpenGLRenderer() {
	
}

void OpenGLRenderer::render()
{
	this->updateGL();
}

void OpenGLRenderer::initialize()
{
	
}

void OpenGLRenderer::initializeGL()
{
	qglClearColor(Qt::white);

	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	//glEnable(GL_NORMALIZE);

	GLfloat ambient_light[4] =
	{
		0.2f, 0.2f, 0.2f, 1.0f
	};


	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_light);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT);
	static GLfloat lightPosition[4] = { -100.0f, 0.0f, 100.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
}

void OpenGLRenderer::resizeGL(int width, int height)
{
	if (width > height)
		QGLWidget::resize(QGLWidget::height(), QGLWidget::height());
	else
		QGLWidget::resize(QGLWidget::width(), QGLWidget::width());

	int side = qMin(width, height);
	glViewport(0, 0, side, side);
	glMatrixMode(GL_PROJECTION);
	//reset View.
	glLoadIdentity();
	glOrtho(- model->getPerspective(), model->getPerspective(), - model->getPerspective(), model->getPerspective() , -model->getPerspective() * 10, model->getPerspective() * 10);
	glMatrixMode(GL_MODELVIEW);
}

void OpenGLRenderer::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Draw simple quads...

	glBegin(GL_QUADS);
	glColor3f(0, 0, 0);
	glNormal3f(0, 1, 0);
	glVertex3f(-100, 1, -100);
	glVertex3f(100, 1, -100);
	glVertex3f(100, 1, 100);
	glVertex3f(-100, 1, 100);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0, 0, 0);
	glNormal3f(0, -1, 0);
	glVertex3f(-100, 0, -100);
	glVertex3f(100, 0, -100);
	glVertex3f(100, 0, 100);
	glVertex3f(-100, 0, 100);
	glEnd();
	
	for (Mesh3D x : model->getData())
	{
		//	glDisable(GL_LIGHTING);
		glPushMatrix();
		x.render();
		glPopMatrix();
		//	glEnable(GL_LIGHTING);
	}

	model->setQuarter(model->getX() / 90);
	glLoadIdentity();
	glRotatef(model->getX(), 0, 1, 0);
	glRotatef(model->getY() * model->calculateSetY(), 1, 0, 0);
	glRotatef(model->getY() * model->calculateSetX(), 0, 0, 1);
	static GLfloat lightPosition[4] = { -100, 0, 100, 1.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
}

void OpenGLRenderer::wheelEvent(QWheelEvent * event)
{

	model->setPerspective(model->getPerspective() - event->angleDelta().y() / 8 / 5 * 1.0 / 2);
	this->resizeGL(QGLWidget::width(), QGLWidget::height());
}

void OpenGLRenderer::mousePressEvent(QMouseEvent * event)
{

	model->setMouseX( event->x() );
	model->setMouseY(event->y() );
}

void OpenGLRenderer::mouseMoveEvent(QMouseEvent * event)
{

	model->calculateRotation(event->x(), event->y());
}

void OpenGLRenderer::show()
{
	QGLWidget::show();
}


