#pragma once

#include <QtWidgets\QWidget>

class Renderer : public QWidget
{
public:
	Renderer(QWidget *parent = 0) : QWidget(parent) {}
	virtual void render() = 0;
	virtual void initialize() = 0;
	virtual void show() = 0;
};