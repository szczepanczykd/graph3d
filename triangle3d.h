#ifndef TRIANGLE3D_H
#define TRIANGLE3D_H

#include <QtGui\qvector3d.h>

class Triangle3D : public QVector3D
{
public:
    Triangle3D();
    Triangle3D(int x, int y, int z);
    int x();
    int y();
    int z();
    void setX(int x);
    void setY(int y);
    void setZ(int z);
};

#endif // TRIANGLE3D_H
