﻿#include "meshproperties.h"


meshProperties::meshProperties()
{
	_visible = true;
	_ownerName = "";
}

meshProperties::meshProperties(const meshProperties &meshProp)
{

	this->_visible = meshProp.visible();
	this->_ownerName = meshProp.getName();

}

meshProperties::meshProperties(QString name,QWidget * parent) : QWidget(parent) {

	this->_visible = true;
	this->_ownerName = name;
}

meshProperties::~meshProperties() {
	if(visibleBox)
	delete visibleBox;
}
QString meshProperties::getName() const
{
	return _ownerName;
}


bool meshProperties::visible() const
{
	return _visible;
}

void meshProperties::checkboxPressed(bool state)
{
	//if clicked, negate state, and emit value!
	_visible = !_visible;
	emit stateChanged(_ownerName,state);
}

meshProperties& meshProperties::operator= (const meshProperties& meshProp)
{
	this->_visible = meshProp.visible();
	this->_ownerName = meshProp.getName();
	createCheckBox();
	return *this;
}

void meshProperties::createCheckBox()
{
	//just Init CheckBox
	//in future there will be more things.
	visibleBox = new QCheckBox("Visible", this);
	visibleBox->setChecked(_visible);
	visibleBox->show();
	visibleBox->resize(100, 50);
	connect(visibleBox, SIGNAL(toggled(bool)), this, SLOT(checkboxPressed(bool)));
}
