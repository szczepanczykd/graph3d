#ifndef SCENE3D_H
#define SCENE3D_H


#include "QtWidgets\qwidget.h"
#include <QtOpenGL\QGLWidget>
#include <QtCore\QVector>
#include <QtCore\QObject>
#include "mesh3d.h"
#include "object3dfactory.h"
#include <QtCore\QString>
#include <QtCore\QDebug>
#include <QtGui\QMouseEvent>
#include <gl\GLU.h>
#include <QtCore\qmath.h>
#include "abstractObserver.h"
#include "abstractSubject.h"

#include "Renderer.h"
#include "openglrenderer.h"

class Scene3D : public QObject, public abstractObserver
{
    Q_OBJECT
    QVector <Mesh3D> _mesh_vec;
    bool moved;
    float _mouse_clicked_x;
    float _mouse_clicked_y;
	float perspective = 50;
	void draw();
	float x = 0;
	float y = 0;
	int quarter = 0;
	Object3DFactory * factory = 0;
	abstractSubject *subject;

	Renderer *view;

public:
    explicit Scene3D();
    virtual ~Scene3D();

    void insertMeshObject(const Mesh3D &obj);
    void removeMesh(const int index);
	void update();
	void setSubject(abstractSubject *sub);

	float getMouseX();
	float getMouseY();
	float getPerspective();
	float getX();
	float getY();
	int getQuarter();
	QVector <Mesh3D> getData();

	void setMouseX(float mouseX);
	void setMouseY(float mouseY);
	void setPerspective(float perspective);
	void setX(float x);
	void setY(float y);
	void setQuarter(int quarter);

	void calculateRotation(float mouseX, float mouseY);
	float calculateSetX();
	float calculateSetY();

	Renderer*& getView();
	void setView(Renderer *render);

private slots:
	void meshCreated(const Mesh3D &val);

public slots:
    void purgeItems();
    void addFactoryObject(const QString objName);
	void updateLastObjName(const QString name);
	void updateMeshParams(const QString name, const QVector3D &offset, const QVector3D &move, const QVector3D &degrees, const QColor &color);
	void setParams(const QString objName, bool visible);
signals:
    void itemAdded(const QString &name);
    void itemsPurged();
};

#endif // SCENE3D_H
