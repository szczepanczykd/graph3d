#include "scene3d.h"

#define PI 3.14159265

//Scene3D::Scene3D(QWidget * parent) : QGLWidget(QGLFormat(QGL::SampleBuffers), parent), moved(false)
//{
//this->installEventFilter(this);
//this->updateGL();

//}

Scene3D::Scene3D()
{
}

Scene3D::~Scene3D()
{
	qDebug() << "Automatyczne zwalnianie si� zasob�w.";
	if (factory)
		delete factory;
}

void Scene3D::insertMeshObject(const Mesh3D &obj)
{
this->_mesh_vec.push_back(obj);
}


void Scene3D::updateMeshParams(const QString name, const QVector3D &offset,const QVector3D &move, const QVector3D &degrees, const QColor &color)
{
	QVector<Mesh3D>::iterator iter;

	iter = _mesh_vec.begin();

	for(iter = _mesh_vec.begin() ; iter != _mesh_vec.end(); iter++)
	{
		if (iter->getName() == name)
		{
			iter->setColor(color);
			iter->setOffset(offset);
			iter->setMove(move);
			iter->setDegrees(degrees);
			view->render();
			return;
		}
	}
}

void Scene3D::setParams(const QString objName, bool visible)
{
	QVector<Mesh3D>::iterator iter;

	iter = _mesh_vec.begin();

	for (iter = _mesh_vec.begin(); iter != _mesh_vec.end(); iter++)
	{
		if (iter->getName() == objName)
		{
			iter->setState(visible);
			view->render();
			return;
		}
	}
}

void Scene3D::removeMesh(const int index)
{
if(index < this->_mesh_vec.size() && index >= 0)
{
    this->_mesh_vec.removeAt(index);
}

}

void Scene3D::update()
{
	qDebug() << "Color has been changed!";
	qDebug() << subject->action();
}

void Scene3D::setSubject(abstractSubject * sub)
{
	this->subject = sub;
}

float Scene3D::getMouseX()
{
	return _mouse_clicked_x;
}

float Scene3D::getMouseY()
{
	return _mouse_clicked_y;
}

float Scene3D::getPerspective()
{
	return perspective;
}

float Scene3D::getX()
{
	return x;
}

float Scene3D::getY()
{
	return y;
}

int Scene3D::getQuarter()
{
	return this->x / 90;
}

QVector<Mesh3D> Scene3D::getData()
{
	return _mesh_vec;
}

void Scene3D::setMouseX(float mouseX)
{
	this->_mouse_clicked_x = mouseX;
}

void Scene3D::setMouseY(float mouseY)
{
	this->_mouse_clicked_y = mouseY;
}

void Scene3D::setPerspective(float perspective)
{
	this->perspective = perspective;
	view->render();
}

void Scene3D::setX(float x)
{
	this->x = x;
}

void Scene3D::setY(float y)
{
	this->y = y;
}

void Scene3D::setQuarter(int quarter)
{
	this->quarter = quarter;
}

void Scene3D::calculateRotation(float mouseX, float mouseY)
{
	x += (mouseX - _mouse_clicked_x) * 0.2;

	if (quarter == 2 || quarter == 3)
	{
		y -= (mouseY - _mouse_clicked_y) * 0.2;
	}
	else
	{
		y += (mouseY - _mouse_clicked_y) * 0.2;
	}

	this->_mouse_clicked_x = mouseX;
	this->_mouse_clicked_y = mouseY;

	if (x > 359)
		x = 0;
	if (y > 179)
		y = -180;
	if (x < 0)
		x = 359;
	if (y < -180)
		y = 179;

	view->render();
}

float Scene3D::calculateSetX()
{
	float setx = x / 90 - quarter;
	float sety = 1.0 - setx;

	if (quarter % 2 == 1)
	{
		sety = x / 90.0 - quarter;
		setx = 1.0 - setx;
	}
	return setx;
}

float Scene3D::calculateSetY()
{
	float setx = x / 90 - quarter;
	float sety = 1.0 - setx;

	if (quarter % 2 == 1)
	{
		sety = x / 90.0 - quarter;
		setx = 1.0 - setx;
	}
	return sety;
}

Renderer *& Scene3D::getView()
{
	return view;
}

void Scene3D::setView(Renderer * render)
{
	view = render;
	view->render();
}





void Scene3D::meshCreated(const Mesh3D &val)
{
	this->insertMeshObject(val);
	view->render();
	factory->close();
	emit itemAdded(_mesh_vec.last().getName());
}


void Scene3D::purgeItems()
{
    this->_mesh_vec.clear();
	glPopMatrix();
	glLoadIdentity();
	view->render();
    emit this->itemsPurged();
}

void Scene3D::updateLastObjName(const QString name)
{
	_mesh_vec.last().setName(name);
}

void Scene3D::addFactoryObject(const QString objName)
{
	if (factory)
		delete factory;
	factory = new Object3DFactory(objName);
	factory->show();
	connect(factory, SIGNAL(objectCreated(const Mesh3D &)), this, SLOT(meshCreated( const Mesh3D &)));

}
