# README #

Simple **Qt5** graphic program. Using OpenGL and QtWidgets.

You can compile it with Qt Creator or Visual Studio.

Just create new Project and add sources.


In pro file, you have to put:

**QT += core gui OpenGL widgets**

In linker flags:

**-glu32** **-opengl32**

or use **CMake**:

-Create *QTDIR* environment variable which *points to QT instalation*. Eg. **C:\Qt\5.6\msvc2015_64**

-create build folder for binaries. Eg. build

-execute CMake: 

**/c/CMake/bin/cmake.exe -G "Visual Studio 14 2015 Win64" -DCMAKE_PREFIX_PATH=$QTDIR ..** 

-then build all.

** /c/CMake/bin/cmake.exe --build .  --config Release**



You have to put OpenGL lib, includes, bin files to:

QTDIR/include/

QTDIR/bin/

QTDIR/lib/


You can download it from

[GLUT official page](https://www.opengl.org/resources/libraries/glut/glut_downloads.php)

[FreeGLUT](http://freeglut.sourceforge.net/)

[GLEW official page](http://glew.sourceforge.net/)

[Other stuff](https://mycodelog.com/2010/05/15/gllibs/)