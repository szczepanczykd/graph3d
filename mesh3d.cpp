#include "mesh3d.h"

Mesh3D::Mesh3D()
{
}

Mesh3D::Mesh3D(const QString name) : _objName(name)
{
this->state = true;
this->_objectColor.setRed(255); //Default value is green.
this->_objectColor.setGreen(0); 
this->_objectColor.setBlue(0);
_offset.setX(0); //Offset is zero!
_offset.setY(0);
_offset.setZ(0);
}

void Mesh3D::render()
{
	if (state)
	{
		//set draving color!
		glColor3f(_objectColor.red() * 1.0 / 255, _objectColor.green() * 1.0 / 255, _objectColor.blue() * 1.0 / 255);
		//translate local matrix
		glTranslatef(_offset.x() + _move.x(), _offset.y() + _move.y(), _offset.z() + _move.z());
		//rotate local world!
		glRotatef(_degrees.z(), 0, 0, 1);
		glRotatef(_degrees.y(), 0, 1, 0);
		glRotatef(_degrees.x(), 1, 0, 0);

		//draving loop.
		for (int i = 0; i < _point3DVec.size(); i += 4)
		{
			glBegin(GL_TRIANGLES);
			glNormal3f(_point3DVec.at(i)[0], _point3DVec.at(i)[1], _point3DVec.at(i)[2]);
			glVertex3f(_point3DVec.at(i + 1)[0], _point3DVec.at(i + 1)[1], _point3DVec.at(i + 1)[2]);
			glVertex3f(_point3DVec.at(i + 2)[0], _point3DVec.at(i + 2)[1], _point3DVec.at(i + 2)[2]);
			glVertex3f(_point3DVec.at(i + 3)[0], _point3DVec.at(i + 3)[1], _point3DVec.at(i + 3)[2]);
			glEnd();
		}
		//force execution of gl commands!
		glFlush();
	}
}

void Mesh3D::insertPoint(const Point3D &point)
{
    this->_point3DVec.push_back(point);
}

void Mesh3D::insertTriangle(const Triangle3D &triangle)
{
    this->_triangle3DVec.push_back(triangle);
}

void Mesh3D::insertOffset(const QVector3D &offset)
{
    this->_offset = offset;
}

void Mesh3D::setName(const QString &objName)
{
    this->_objName = objName;
}

void Mesh3D::setState(bool state)
{
    this->state = state;
}

void Mesh3D::setColor(QColor color)
{

	if (color.isValid()) //isValid == other that rgb (0,0,0) or CMYK (0,0,0,0)
	{
		this->_objectColor.setRed(color.red());
		this->_objectColor.setGreen(color.green());
		this->_objectColor.setBlue(color.blue());
	}
}

void Mesh3D::setOffset(QVector3D offset)
{
	if (offset.isNull() == false)
		this->_offset = offset;
}

void Mesh3D::setDegrees(QVector3D degrees)
{
	this->_degrees += degrees; //We are adding values! Not replace them !!!
}

void Mesh3D::setMove(QVector3D move)
{
	this->_move += move; 
}

Point3D Mesh3D::pointAt(int i)
{

	if (i < _point3DVec.size() && i >= 0)
		return _point3DVec.at(i);
	return Point3D();
}

bool Mesh3D::isEnabled()
{
    return this->state; //enabled means : visible and allowed for rendering.
}

QString Mesh3D::getName()
{
	return this->_objName;
}