#ifndef MAINWND_H
#define MAINWND_H

#include <QtWidgets\QMainWindow>
#include <QtWidgets\QPushButton>
#include <QtWidgets\QMenu>
#include <QtWidgets\QMenuBar>
#include <QtCore\QScopedPointer>
#include <QtWidgets\QFileDialog>
#include <QtWidgets\QAction>
#include "custtreeview.h"
#include "scene3d.h"
#include "stlfilereader.h"
#include <QtCore\QSignalMapper>
#include <QtCore\QDebug>
#include <QtWidgets\qboxlayout.h>
#include "openglrenderer.h"
#include "Renderer.h"
namespace Ui {
class MainWnd;
class Scene3D;
}

class MainWnd : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWnd(QWidget *parent = 0);
    ~MainWnd();

private:
	CustTreeView *custTreeView;
	Scene3D *scene;
	QPushButton *createCube;
	QPushButton *createCone;
	Renderer *renderer;
	//Ui::MainWnd *ui = 0;
    QSignalMapper *mapper;
private slots:
    void selectFile();
    void appendItemToTree(const QString fileDir);
signals:
    void fileChoosen(const QString &filedir);
};

#endif // MAINWND_H
